﻿Function Get-VolumeFreeSpace {
[CmdletBinding()]
    Param(
        [Parameter(
            ValueFromPipeline = $True,
            Position = 0
        )]
        [String[]]
        $ComputerName = $env:computername
    )
    
    Begin {
        $invoke_script = {
            $check = Get-Command -Name "Get-Volume" -ErrorAction SilentlyContinue | Out-Null

            If($check) {
                $drives = Get-Volume | 
                Where-Object { $_.DriveLetter -and $_.DriveType -eq 'Fixed'} | 
                Select-Object DriveLetter,FileSystemLabel,Size,SizeRemaining
            } Else {
                $options = @{
                    'Class' = "Win32_LogicalDisk"
                    'Property' = "DeviceId","VolumeName","Size","FreeSpace"
                    'Filter' = "DriveType = 3"
                }
                $drives = Get-WmiObject @options |
                Select-Object @{
                    'Name' = 'DriveLetter'
                    'Expression' = {$_.DeviceId[0]}
                }, @{
                    'Name' = 'FileSystemLabel'
                    'Expression' = {$_.VolumeName}
                
                }, Size, @{
                    'Name' = 'SizeRemaining'
                    'Expression' = {$_.FreeSpace}
                }
            }

            $drives |
            Select-Object DriveLetter, FileSystemLabel, @{
                'Name' = 'FreeSpace'
                'Expression' = {
                    "{0:N0} GB ({1:N0}%)" -f @(
                        ($_.SizeRemaining / 1GB)
                        ($_.SizeRemaining / $_.Size) * 100
                    )
                }
            }, @{
                'Name' = 'TotalSize'
                'Expression' = {
                    "{0:N0} GB" -f ($_.Size / 1GB)
                }
            }
        }

        $this_computer = @(
            $env:computername
            [System.Net.Dns]::GetHostByName($env:computername).HostName
            'localhost'
        )
    }
    
    Process {
        ForEach($computer in $ComputerName) {

            $options = @{
                'ScriptBlock' = $invoke_script
            }

            If ($this_computer -notcontains $computer) {
                $options['ComputerName'] = $computer
            }

            Invoke-Command @options |
            Select-Object -Property @{
                'Name'='ComputerName'
                'Expression'={$computer}
            },DriveLetter,FileSystemLabel,FreeSpace,TotalSize
        }
    }
}

Function Set-AlternatingRows {
	<#
	.SYNOPSIS
		Simple function to alternate the row colors in an HTML table
	.DESCRIPTION
		This function accepts pipeline input from ConvertTo-HTML or any
		string with HTML in it.  It will then search for <tr> and replace 
		it with <tr class=(something)>.  With the combination of CSS it
		can set alternating colors on table rows.
		
		CSS requirements:
		.odd  { background-color:#ffffff; }
		.even { background-color:#dddddd; }
		
		Classnames can be anything and are configurable when executing the
		function.  Colors can, of course, be set to your preference.
		
		This function does not add CSS to your report, so you must provide
		the style sheet, typically part of the ConvertTo-HTML cmdlet using
		the -Head parameter.
	.PARAMETER Line
		String containing the HTML line, typically piped in through the
		pipeline.
	.PARAMETER CSSEvenClass
		Define which CSS class is your "even" row and color.
	.PARAMETER CSSOddClass
		Define which CSS class is your "odd" row and color.
	.EXAMPLE $Report | ConvertTo-HTML -Head $Header | Set-AlternateRows -CSSEvenClass even -CSSOddClass odd | Out-File HTMLReport.html
	
		$Header can be defined with a here-string as:
		$Header = @"
		<style>
		TABLE {border-width: 1px;border-style: solid;border-color: black;border-collapse: collapse;}
		TH {border-width: 1px;padding: 3px;border-style: solid;border-color: black;background-color: #6495ED;}
		TD {border-width: 1px;padding: 3px;border-style: solid;border-color: black;}
		.odd  { background-color:#ffffff; }
		.even { background-color:#dddddd; }
		</style>
		"@
		
		This will produce a table with alternating white and grey rows.  Custom CSS
		is defined in the $Header string and included with the table thanks to the -Head
		parameter in ConvertTo-HTML.
	.NOTES
		Author:         Martin Pugh
		Twitter:        @thesurlyadm1n
		Spiceworks:     Martin9700
		Blog:           www.thesurlyadmin.com
		
		Changelog:
			1.1         Modified replace to include the <td> tag, as it was changing the class
                        for the TH row as well.
            1.0         Initial function release
	.LINK
		http://community.spiceworks.com/scripts/show/1745-set-alternatingrows-function-modify-your-html-table-to-have-alternating-row-colors
    .LINK
        http://thesurlyadmin.com/2013/01/21/how-to-create-html-reports/
	#>
    [CmdletBinding()]
   	Param(
       	[Parameter(Mandatory,ValueFromPipeline)]
        [string]$Line,
       
   	    [Parameter(Mandatory)]
       	[string]$CSSEvenClass,
       
        [Parameter(Mandatory)]
   	    [string]$CSSOddClass
   	)
	Begin {
		$ClassName = $CSSEvenClass
	}
	Process {
		If ($Line.Contains("<tr><td>"))
		{	$Line = $Line.Replace("<tr>","<tr class=""$ClassName"">")
			If ($ClassName -eq $CSSEvenClass)
			{	$ClassName = $CSSOddClass
			}
			Else
			{	$ClassName = $CSSEvenClass
			}
		}
		Return $Line
	}
}

Function ConvertTo-CustomHtml {
[CmdletBinding()]
    Param(
        [Parameter(
            Mandatory = $True,
            ValueFromPipeline = $True
        )]
        [Object[]]
        $InputObject,

        [String]
        $Title,

        [String]
        $Body,

        [String]
        $PostContent,

        [String]
        $PreContent,

        [String[]]
        $Property
    )
    
    Begin {
        $Head = @"
<style>
    table {
        margin:0px;
        padding:2px;
        border: 5px solid black;
        border-collapse: collapse;
        width: 100%;
        height: 100%;
    } 
    tr.odd {
        background-color:#00FFCC;
    }
    tr.even {
        background-color:#0099CC;
    }
    td,th {
        border: 2px solid black;
        vertical-align:middle;
        text-align:left;
    }
    th {
        background-color:#101010;
        color:#D8D8D8;
    }
</style>
"@
        If($Title) { $Head = "<title>$Title</title>`r`n$Head" }

        $params = @{
            'Head' = $Head
        }
        
        If($Body) { $params['Body'] = $Body }
        If($PostContent) { $params['PostContent'] = $PostContent }
        If($PreContent) { $params['PreContent'] = $PreContent }
        If($Property) { $params['Property'] = $Property }

        $objs = New-Object -TypeName System.Collections.ArrayList
    }
    
    Process {
        ForEach ($obj in $InputObject) { $objs.Add($obj) | Out-Null }
    }

    End {
        $objs | 
        ConvertTo-Html @params | 
        Set-AlternatingRows -CSSEvenClass 'even' -CSSOddClass 'odd'
    }
}

Function ConvertTo-Base64 ($string) {
    $bytes = [System.Text.Encoding]::UTF8.GetBytes($string)
    [System.Convert]::ToBase64String($bytes)
}

Function ConvertFrom-Base64 ($string) {
    $bytes = [System.Convert]::FromBase64String($string)
    [System.Text.Encoding]::UTF8.GetString($bytes)

}

Function Get-ItemSize 
{
    [cmdletbinding()]

    Param(
        [Parameter( Mandatory         = $True,
                    Position          = 0,
                    ValueFromPipeline = $True )]
        [ValidateScript({ "String","DirectoryInfo","FileInfo" -contains 
                          $_.GetType().Name })]
        $Path,

        [Int]
        $Round = 2,

        [switch]
        $Raw,

        [switch]
        $Display
    )

    Begin
    {
        $tier = @(
            @{'Value' = 1KB; 'Label' = 'KB'}
            @{'Value' = 1MB; 'Label' = 'MB'}
            @{'Value' = 1GB; 'Label' = 'GB'}
            @{'Value' = 1TB; 'Label' = 'TB'}
        )

        If($Round -ge 0)
        {
            $round_fmt = ":N$Round"
        }
    }

    Process
    {
        Switch ($Path.GetType().Name)
        {
            "DirectoryInfo"
            {
                $ItemPath = $Path.FullName
            }
            "FileInfo"
            {
                $ItemPath = $Path.FullName
                Break
            }
            "String"
            {
                $ItemPath = $Path
                Break
            }
        }

        $item = Get-Item -Path $ItemPath

        Switch ($item.PSIsContainer) 
        {
            
            $True
            {          
                $size = Get-ChildItem -Path $item.FullName -Recurse -ErrorAction SilentlyContinue -ErrorVariable x |
                Measure-Object -Property Length -Sum |
                Select-Object -ExpandProperty Sum

                If($x)
                {
                    $adWarningMessage = " (Size Unreliable: {0})" -f ( ( $x | 
                                                                         Select-Object -ExpandProperty Exception | 
                                                                         ForEach-Object { $_.GetType().Name } | 
                                                                         Select-Object -Unique ) -join ',' )
                    Clear-Variable -Name x -ErrorAction SilentlyContinue
                }
            }

            $False
            {
                $size = $item.Length
            }
        }

        If( $Raw )
        {
            $size
        }
        Else
        {
            $output = $size
            If($size) 
            {
                $label = 'B'
            }
            Else
            {
                $label = ''
            }

            For ( $i = 0; $i -lt $tier.Length; $i++ )
            {
                If ( $size / $tier[$i]['Value'] -ge 1 )
                {
                    $output = $size / $tier[$i]['Value']
                    $label = $tier[$i]['Label']
                }
                Else
                {
                    Break
                }
            }

            $display_msg = "{0$round_fmt} {1}$adWarningMessage" -f $output,$label
            Clear-Variable -Name adWarningMessage -ErrorAction SilentlyContinue

            If( $Display )
            {
                $display_msg
            }
            Else
            {
                New-Object -TypeName PSObject -Property @{ 'Size' = $size
                                                           'SizeDisplay' = $display_msg }
            }
        }
    }

}

Function Format-Byte {
    [cmdletbinding(DefaultParameterSetName = 'Soft')]

    Param(
        [Parameter( Mandatory = $True,
                    ValueFromPipeline = $True,
                    Position = 0 )]
        [Int64]$Number,

        [ValidateScript({
            $_ -ge 0 -and
            $_ -le 15
        })]
        [Int]$Round = 2,

        [Switch]$NoRound,

        [Parameter( Mandatory = $True,
                    ParameterSetName = 'Hard',
                    Position = 1 )]
        [ValidateSet('KB','MB','GB','TB','PB')]
        [String]
        $Multiplier
    )

    Begin {
        $multipliers = @(
            'KB'
            'MB'
            'GB'
            'TB'
            'PB'
        )

        $number_descriptor = 'Bytes'

        $divide = {
            
        }
    }

    Process {
        If($PSCmdlet.ParameterSetName -eq 'Soft') {

            ForEach($multiplier in $multipliers) {

                If(($Number / (Invoke-Expression "1$multiplier")) -ge 1) {

                    $number_descriptor = $multiplier

                } Else {

                    Break

                }

            }

        } Else {

            $number_descriptor = $Multiplier

        }

        If($number_descriptor -ne 'Bytes') {

            $number_out = $Number / (Invoke-Expression "1$number_descriptor")

        } Else {

            $number_out = $Number

        }

        If(-not $NoRound) {

            $number_out = [Math]::Round($number_out,$Round)

        }

        $string = "{0} {1}" -f $number_out,$number_descriptor

        $string
    }
} # End Format-Byte