﻿Function ConvertTo-MultiMarkdownFromMediaWiki
{
    [cmdletbinding()]

    Param(
        [Parameter( ValueFromPipeline = $True )]
        [String]
        $InputObject
    )

    Begin
    {
        $pandoc = "pandoc.exe"
        $options = @(
            '-f'
            'mediawiki'
            '-t'
            'markdown_mmd'
            '--atx-headers'
        )

        $collector = New-Object -TypeName 'System.Collections.ArrayList'
    }

    Process
    {
        $collector.Add($InputObject) | Out-Null
    }

    End
    {
        $collector | &$pandoc $options
    }

} # Function ConvertTo-MultiMarkdownFromMediaWiki
