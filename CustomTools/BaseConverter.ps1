﻿Function ConvertTo-NumberBase
{
    [CmdletBinding()]

    Param(
        [Parameter( Mandatory         = $True,
                    Position          = 0,
                    ValueFromPipeline = $True )]
        [ValidateScript({
            $inObj = $_
            Switch ($_.GetType().Name)
            {
                "Int32" 
                { 
                    $True
                }
                "String"
                {
                    # Pattern will match
                    # Hex values less than or equal to
                    # [Int32]::MaxValue
                    # with or without the leading 0x
                    $inObj -match '^(?:0[xX])?[0-7]?[0-9a-fA-F]{1,7}$' -or

                    # Pattern will match
                    # Binary values less than or equal to
                    # [Int32]::MaxValue
                    $inObj -match '^(?:0)*(1|0){1,31}$'
                }
                Default
                {
                    $False
                }
            }
        })]
        [Object]
        $InputObject,

        [ValidateSet( 'Decimal',
                      'Binary',
                      'Hexadecimal',
                      'Octal' )]
        [String]
        $OutputValue
    )

    Process
    {
        Switch ($InputObject.GetType().Name)
        {
            "Int32" 
            { 
                 $result = New-Object -TypeName PSObject -Property @{
                    'Decimal'     = $InputObject
                    'Binary'      = [Convert]::ToString($InputObject,2)
                    'Hexadecimal' = [Convert]::ToString($InputObject,16) 
                    'Octal'       = [Convert]::ToString($InputObject,8)  }
            }
            "String"
            {
                Switch -Regex ($InputObject)
                {
                    '^(?:0|1)+$'
                    {
                        $int = [Convert]::ToInt32($_,2)

                        $result = New-Object -TypeName PSObject -Property @{
                            'Decimal'     = $int
                            'Binary'      = $_
                            'Hexadecimal' = [Convert]::ToString($int,16)
                            'Octal'       = [Convert]::ToString($int,8)
                        }
                        Break
                    }
                    '^(?:0x)?[0-9a-fA-F]+$'
                    {
                        If ($_.StartsWith('0x') )
                        {
                            $hex = $_.substring(2)
                        }
                        Else
                        {
                            $hex = $_
                        }

                        $int = [Convert]::ToInt32($hex,16)

                        $result = New-Object -TypeName PSObject -Property @{
                            'Decimal'     = $int
                            'Binary'      = [Convert]::ToString($int,2)
                            'Hexadecimal' = $hex
                            'Octal'       = [Convert]::ToString($int,8)
                        }
                        Break
                    }
                } # End switch for string input

            }
            Default
            {
                $False
            }
        } # End switch for building result

        If( $OutputValue )
        {
            $result |
            Select-Object -ExpandProperty $OutputValue
        }
        Else
        {
            $result
        }
    }
}