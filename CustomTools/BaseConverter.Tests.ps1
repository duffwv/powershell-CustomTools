﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. "$here\$sut"

Describe "ConvertTo-NumberBase" {

    Function Test-ConvertTo
    {
        Param(
            [Parameter( Mandatory         = $True,
                        Position          = 0,
                        ValueFromPipeline = $True )]
            [Object]
            $InputObject,

            [Parameter( Mandatory = $True )]
            [ValidateSet( 'Decimal',
                          'Binary',
                          'Hexadecimal',
                          'Octal' )]
            [String]
            $InputValue,

            [Parameter( Mandatory = $True )]
            [ValidateSet( 'Decimal',
                          'Binary',
                          'Hexadecimal',
                          'Octal' )]
            [String]
            $OutputValue
        )

        Begin
        {
            $base = @{
                'Binary'      = 2
                'Hexadecimal' = 16
                'Octal'       = 8
            }
        }

        Process
        {
            $result = $InputObject |
            ConvertTo-NumberBase -OutputValue $OutputValue

            If($InputValue -eq 'Decimal')
            {
                If($InputValue -ne $OutputValue)
                {
                    $result |
                    Should Be ( [Convert]::ToString($InputObject,$base[$OutputValue]) )
                }
            }
            Else
            {
                $int = [Convert]::ToInt32($InputObject,$base[$InputValue])

                Switch ($OutputValue)
                {
                    'Decimal'
                    {
                        $result | 
                        Should Be $int
                    }

                    Default
                    {
                        $result |
                        Should Be ( [Convert]::ToString($int,$base[$_]) )
                    }
                }
            }

        }
    }

    It "converts integers to binary" {
        1..10 |
        % { Get-Random } |
        Test-ConvertTo -InputValue Decimal -OutputValue Binary

        10 |
        ConvertTo-NumberBase -OutputValue Binary |
        Should Be "1010"
    }

    It "converts integers to hexadecimal" {
        1..10 |
        % { Get-Random } |
        Test-ConvertTo -InputValue Decimal -OutputValue Hexadecimal

        10 |
        ConvertTo-NumberBase -OutputValue Hexadecimal |
        Should Be 'a'
    }

    It "converts integers to octal" {
        1..10 |
        % { Get-Random } |
        Test-ConvertTo -InputValue Decimal -OutputValue Octal

        10 |
        ConvertTo-NumberBase -OutputValue Octal |
        Should Be '12'
    }

    It 'converts binary to integers' {
        1..10 | 
        %{ Get-Random } |
        ConvertTo-NumberBase -OutputValue Binary |
        Test-ConvertTo -InputValue Binary -OutputValue Decimal

        '1010' |
        ConvertTo-NumberBase -OutputValue Decimal |
        Should Be 10
    }

    It 'converts binary to hex' {
        1..10 | 
        %{ Get-Random } |
        ConvertTo-NumberBase -OutputValue Binary |
        Test-ConvertTo -InputValue Binary -OutputValue Hexadecimal

        '1010' |
        ConvertTo-NumberBase -OutputValue Hexadecimal |
        Should Be 'a'
    }

    It 'converts binary to octal' {
        1..10 | 
        %{ Get-Random } |
        ConvertTo-NumberBase -OutputValue Binary |
        Test-ConvertTo -InputValue Binary -OutputValue Octal

        '1010' |
        ConvertTo-NumberBase -OutputValue Octal |
        Should Be '12'
    }

    It 'converts hex to integers' {
        1..10 | 
        %{ Get-Random } |
        ConvertTo-NumberBase -OutputValue Hexadecimal |
        Test-ConvertTo -InputValue Hexadecimal -OutputValue Decimal

        'a' |
        ConvertTo-NumberBase -OutputValue Decimal |
        Should Be 10
    }

    It 'converts hex to binary' {
        1..10 | 
        %{ Get-Random } |
        ConvertTo-NumberBase -OutputValue Hexadecimal |
        Test-ConvertTo -InputValue Hexadecimal -OutputValue Binary

        'a' |
        ConvertTo-NumberBase -OutputValue Binary |
        Should Be '1010'
    }

    It 'converts hex to octal' {
        1..10 | 
        %{ Get-Random } |
        ConvertTo-NumberBase -OutputValue Hexadecimal |
        Test-ConvertTo -InputValue Hexadecimal -OutputValue Octal

        'a' |
        ConvertTo-NumberBase -OutputValue Octal |
        Should Be '12'
    }

    It 'has regex that validates hex in Int32 range' {
        
        $goodvalues = 0,1,[int32]::MaxValue + (1..10 | %{ Get-Random -Minimum 2 -Maximum ([int32]::MaxValue - 1) }) |
        ForEach-Object {
            [Convert]::ToString($_,16)
        }

        $badvalues = -1,([int32]::MaxValue + 1)|
        ForEach-Object {
            [Convert]::ToString($_,16)
        }
        
        # Validate test for regex pattern used in
        # function
        
        $regex = '^(?:0[xX])?[0-7]?[0-9a-fA-F]{1,7}$'

        $test = $True

        ForEach ($i in $goodvalues) 
        {
            If( $i -notmatch $regex )
            {
                $test = $False
                Break
            }
        }

        ForEach ($i in $badvalues)
        {
            If( $i -match $regex )
            {
                $test = $False
                Break
            }
        }

        # Test actual function

        $test |
        Should Be $True

        {
            $goodvalues |
            ConvertTo-NumberBase -ErrorAction Stop
        } |
        Should Not Throw

        {
            $badvalues |
            ConvertTo-NumberBase -ErrorAction Stop 
        } |
        Should Throw

    }

    It 'has regex that validates binary in Int32 range' {
        $goodvalues = 0,1,[int32]::MaxValue + (1..10 | %{ Get-Random -Minimum 2 -Maximum ([int32]::MaxValue - 1) }) |
        ForEach-Object {
            [Convert]::ToString($_,2)
        }

        $badvalues = -1,([int32]::MaxValue + 1)|
        ForEach-Object {
            [Convert]::ToString($_,2)
        }        

        # Validate test for regex pattern used in
        # function
        
        $regex = '^(?:0)*(1|0){1,31}$'

        $test = $True

        ForEach ($i in $goodvalues) 
        {
            If( $i -notmatch $regex )
            {
                $test = $False
                Break
            }
        }

        ForEach ($i in $badvalues)
        {
            If( $i -match $regex )
            {
                $test = $False
                Break
            }
        }

        # Test actual function

        $test |
        Should Be $True

        {
            $goodvalues |
            ConvertTo-NumberBase -ErrorAction Stop
        } |
        Should Not Throw

        {
            $badvalues |
            ConvertTo-NumberBase -ErrorAction Stop 
        } |
        Should Throw
    }
}
