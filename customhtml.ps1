Function ConvertTo-CustomHtml2 {
[CmdletBinding()]
    Param(
        [Parameter(
            Mandatory = $True,
            ValueFromPipeline = $True
        )]
        [Object[]]
        $InputObject,

        [String]
        $Title,
		
		[String]
		$Head,
		
		[Switch]
		$Fragment
    )
    
    Begin {
        $head_tag = @"
<style>
    table {
        margin:0px;
        padding:2px;
        border: 5px solid black;
        border-collapse: collapse;
        width: 100%;
        height: 100%;
    } 
    tr.odd {
        background-color:#00FFCC;
    }
    tr.even {
        background-color:#0099CC;
    }
    td,th {
        border: 2px solid black;
        vertical-align:middle;
        text-align:left;
    }
    th {
        background-color:#101010;
        color:#D8D8D8;
    }
</style>
"@

		If($Head) { $head_tag = "$head_tag`r`n$head" }
		If($Title) { $head_tag = "<title>$title</title>`r`n$head_tag" }
		
        $objs = New-Object -TypeName System.Collections.ArrayList
    }
    
    Process {
        ForEach ($obj in $InputObject) { $objs.Add($obj) | Out-Null }
    }

    End {
	
		$PSBoundParameters.Remove('Title') | Out-Null
		$PSBoundParameters.Remove('Head') | Out-Null
		$PSBoundParameters.Remove('InputObject') | Out-Null
		
        $objs | 
        ConvertTo-Html @PSBoundParameters | 
        Set-AlternatingRows -CSSEvenClass 'even' -CSSOddClass 'odd'
    }
}